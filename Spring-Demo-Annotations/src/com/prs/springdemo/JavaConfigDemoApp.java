package com.prs.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JavaConfigDemoApp {

    public static void main(String[] args)
    {
        //Read spring config java class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
        
        //Get the bean from the spring container
        Coach theCoach = context.getBean("tennisCoach" , Coach.class);
        
        //Call a method on the bean.
        System.out.println(theCoach.getDailyWorkout());
        
        System.out.println(theCoach.getDailyFortune());
        
        //Close the container.
        context.close();
    }

}
