package com.prs.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SwimJavaConfigDemoApp {

    public static void main(String[] args)
    {
        //Read spring config java class
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
        
        //Get the bean from the spring container
        SwimCoach theCoach = context.getBean("swimCoach" , SwimCoach.class);
        
        //Call a method on the bean.
        System.out.println(theCoach.getDailyWorkout());
        
        System.out.println(theCoach.getDailyFortune());
        
        System.out.println(theCoach.getEmail());
        
        System.out.println(theCoach.getTeamName());
        
        //Close the container.
        context.close();
    }

}
