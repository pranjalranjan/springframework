package com.prs.springdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

    @Autowired
    @Qualifier("randomFortuneService")
    private FortuneService fortuneService;
    
    public TennisCoach()
    {
        System.out.println("In the default constructor for : Tennis Coach");
    }

//    @Autowired
//    public TennisCoach(@Qualifier("happyFortuneService") FortuneService theFortuneService)
//    {
//        fortuneService = theFortuneService;
//    }
    
    @Override
    public String getDailyWorkout()
    {
        return "Practice your backhand volley.";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }
//
//    @Autowired
//    public void setFortuneService(FortuneService theFortuneService)
//    {
//        fortuneService = theFortuneService;
//    }
    
    //Define init method
    @PostConstruct
    public void initTennisCoach()
    {
        System.out.println("Inside init method for TennisCoach");
    }
    
    @PreDestroy
    public void cleanupTennisCoach()
    {
        System.out.println("Inside pre destroy method for TennisCoach.");
    }
}
