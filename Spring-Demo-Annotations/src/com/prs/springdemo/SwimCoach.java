package com.prs.springdemo;

import org.springframework.beans.factory.annotation.Value;

public class SwimCoach implements Coach
{

    @Value("${team.email}")
    private String email;
    
    @Value("${team.name}")
    private String teamName;
    
    private FortuneService fortuneService;

    //Needed for constructor injection.
    public SwimCoach(FortuneService theFortuneService)
    {
        fortuneService = theFortuneService;
    }
    
    @Override
    public String getDailyWorkout()
    {
        return "Swim for 1 hr straight!";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }

    public String getEmail()
    {
        return email;
    }

    public String getTeamName()
    {
        return teamName;
    }
    
}
