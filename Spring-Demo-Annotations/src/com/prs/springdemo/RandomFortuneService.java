package com.prs.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService
{

    //Create an array of strings
    private String[] fortunes = {
            "Today is not your lucky day!",
            "Be alert today!",
            "Be Happy! You are lucky."
    };
    
    private Random myRandom = new Random();
    
    @Override
    public String getFortune()
    {
        //Pick a random string from the array.
        
        int index = myRandom.nextInt(fortunes.length);
        return fortunes[index];
    }

}
