package com.prs.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//@ComponentScan("com.prs.springdemo")
@PropertySource("classpath:sport.properties")
public class SportConfig
{
  
    //Define bean for sad fortune service. The method name becomes the bean id.
    @Bean
    public FortuneService sadFortuneService()
    {
        return new SadFortuneService();
    }
    
    //Define bean for swim coach and inject dependency. Constructor injection is used.
    @Bean
    public SwimCoach swimCoach()
    {
        return new SwimCoach(sadFortuneService());
    }
    
}
