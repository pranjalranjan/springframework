package com.prs.springdemo;

public interface Coach
{
    public String getDailyWorkout();
    
    public String getDailyFortune();
}
