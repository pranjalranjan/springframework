package com.prs.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeDemoApp
{
    public static void main(String[] args)
    {
        //Load the spring config file.
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
        
        //Retrieve the bean from the spring container
        Coach theCoach = context.getBean("myCoach", Coach.class);
        
        Coach alphaCoach = context.getBean("myCoach", Coach.class);
        
        boolean result = (theCoach == alphaCoach);
        
        System.out.println("Pointing to the same bean : " + result);
        
        System.out.println("Memory Location of theCoach : " + theCoach);
        
        System.out.println("Memory Location of alphaCoach : " + alphaCoach);
        
        context.close();
    }
}
