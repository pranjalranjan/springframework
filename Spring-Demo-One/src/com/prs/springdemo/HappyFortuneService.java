package com.prs.springdemo;

public class HappyFortuneService implements FortuneService {

    @Override
    public String getFortune()
    {
        return "A very good fortune";
    }

}
