package com.prs.springdemo;

public class TrackCoach implements Coach
{

    private FortuneService fortuneService;
    
    public TrackCoach(FortuneService fortuneService)
    {
        this.fortuneService = fortuneService;
    }
    
    public TrackCoach()
    {
    }

    @Override
    public String getDailyWorkout()
    {
        return "Spend 30 mins running!";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }

    //init method
    public void doMyStartup()
    {
        System.out.println("TrackCoach : my custom startup");
    }
    
    //destroy method
    public void doMyCleanup()
    {
        System.out.println("TrackCoach : my custom cleanup");
    }
}
