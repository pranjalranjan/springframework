package com.prs.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifecycleDemoApp
{
    public static void main(String[] args)
    {
        //Load the spring config file.
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle-applicationContext.xml");
        
        //Retrieve the bean from the spring container
        Coach theCoach = context.getBean("myCoach", Coach.class);
        
        System.out.println(theCoach.getDailyWorkout());
        
        context.close();
    }
}
