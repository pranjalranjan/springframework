package com.prs.springdemo;

public class CricketCoach implements Coach {

    private FortuneService fortuneService;
    private String emailAddress;
    private String team;

    public CricketCoach()
    {
        System.out.println("Creating the cricket coach.");
    }

    @Override
    public String getDailyWorkout()
    {
        return "Practice fast bowling for 1 hr";
    }

    @Override
    public String getDailyFortune()
    {
        return fortuneService.getFortune();
    }

    public void setFortuneService(FortuneService fortuneService)
    {
        System.out.println("Inside the setter.");
        this.fortuneService = fortuneService;
    }

    public void setEmailAddress(String emailAddress)
    {
        System.out.println("Inside email address setter.");
        this.emailAddress = emailAddress;
    }

    public void setTeam(String team)
    {
        System.out.println("Inside team setter.");
        this.team = team;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public String getTeam()
    {
        return team;
    }

}
