package com.prs.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController
{
    //Show the initial form.
    @RequestMapping("/showForm")
    public String showForm()
    {
        return "helloworld-form";
    }
    
    //Process the HTML form.
    @RequestMapping("/processForm")
    public String processForm()
    {
        return "helloworld";
    }
    
    @RequestMapping("processFormV2")
    //read form data and add data to the model.
    public String reflectStudentName(HttpServletRequest request, Model model)
    {
        //read the request parameter from the HTML form.
        String theName = request.getParameter("studentName");
        
        //Convert it all to upper case.
        theName = theName.toUpperCase();
        
        //Create the message.
        String result = "Hi " + theName;
        
        //Add the message to the model.
        model.addAttribute("message" , result);
        
        return "helloworld";
    }
    
    @RequestMapping("processFormV3")
    //read form data and add data to the model.
    public String reflectStudentNameFromFormData(@RequestParam("studentName") String theName, Model model)
    {   
        //Convert it all to upper case.
        theName = theName.toUpperCase();
        
        //Create the message.
        String result = "Hi " + theName + " from version 3";
        
        //Add the message to the model.
        model.addAttribute("message" , result);
        
        return "helloworld";
    }
}
