<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>


<title> Student Registration</title>




</head>

<body>

    <form:form action="processForm" modelAttribute="student">
    
    FirstName: <form:input path="firstName"/>
    
    <br><br>
    
    LastName: <form:input path="lastName"/>
    
    <br><br>
    
    Country: <form:select path="country">
        
        <form:options items="${student.countryOptions}"/>
        
    </form:select>
    
    <br><br>
    
    Favourite Language:
    
    Java <form:radiobutton path="favouriteLanguage" value="Java"/>
    C++ <form:radiobutton path="favouriteLanguage" value="C++"/>
    PHP <form:radiobutton path="favouriteLanguage" value="PHP"/>
    C# <form:radiobutton path="favouriteLanguage" value="C#"/>
    
    <br><br>
    
    Operating Systems:
    
    Windows <form:checkbox path="operatingSystems" value="Windows"/>
    Linux <form:checkbox path="operatingSystems" value="Linux"/>
    OSX <form:checkbox path="operatingSystems" value="OSX"/>
    
    <br><br>
    <input type="submit" value="Submit"/>

    </form:form>

</body>




</html>
